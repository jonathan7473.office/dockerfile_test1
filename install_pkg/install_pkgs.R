# ----------------------(START)----------------------
install.packages("png")
install.packages("pheatmap")
install.packages("RCurl")
install.packages("XML")

install.packages("http://hartleys.github.io/QoRTs/QoRTs_STABLE.tar.gz",
                   repos=NULL, 
                   type="source")
# 安裝QoRTs穩定版

install.packages("http://hartleys.github.io/QoRTs/QoRTsExampleData_LATEST.tar.gz",
                 repos = NULL, 
                 type="source")
# 安裝QoRTs example檔

if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install("pasilla", version = "3.8")
BiocManager::install("airway", version = "3.8")
BiocManager::install("vsn", version = "3.8")
BiocManager::install("edgeR", version = "3.8")
BiocManager::install("DESeq2", version = "3.8")
# 從BiocManager安裝各式R packages

# ----------------------(END)----------------------

