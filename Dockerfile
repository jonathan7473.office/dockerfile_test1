# ----------------------(START)----------------------
FROM ubuntu:latest
# 使用最新版的ubuntu當作image的基底機器

ADD install_pkg/install_pkgs.R /tmp/
# 這是將install_pkg/install_pkgs.R放到暫時的container的/tmp/資料夾裡的指令

ENV DEBIAN_FRONTEND=noninteractive
# 環境參數，在建立images的過程中使用

RUN apt-get update
RUN apt-get install wget -y
RUN apt-get install vim -y
RUN apt-get install gnupg2 -y
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
# 此key是R官方伺服器的key，用該key才可登入https的R官方更新伺服器

RUN apt-get install software-properties-common -y
RUN add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran35/"
# 此為R官方更新的網址，將它加入apt查詢的列表當中可以裝到更新的R

RUN apt-get update
RUN apt-get install r-base-core r-base r-base-dev -y
RUN apt-get install libcurl4-openssl-dev libxml2-dev -y
# apt安裝R以及後續R packages會用到的相依性函式庫

RUN apt-get install default-jre -y
RUN apt-get install default-jdk -y
# 這2個RUN指令是要安裝Java執行工具及開發工具

RUN echo "Asia/Taipei" > /etc/timezone
RUN ln -fs /usr/share/zoneinfo/"cat /etc/timezone" /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata
# 這3個RUN指令是要設定image基底ubuntu機器的時區

# 以上這些RUN指令是在建立images時讓暫時產生的container執行的指令

RUN R -f /tmp/install_pkgs.R
# 這個RUN指令是要讓暫時的container執行install_pkgs.R這個R script，該script是要讓該機器的R安裝一系列的R packages


# ----------------------(END)----------------------
